'use strict';

// ================================================================
// get all the tools we need
// ================================================================
var express = require('express');
var routes = require('./routes/admin.js');
var session = require('express-session');
var session = require('client-sessions');

var port = process.env.PORT || 3000;

var app = express();

// ================================================================
// setup our express application
// ================================================================
app.use('/public', express.static(process.cwd() + '/public/assets'));
app.set('view engine', 'ejs');

app.use(session({ cookieName: 'session',secret: 'active-bias',	cookie: {expires: new Date(Date.now() + 60 * 100000),maxAge: 60*10000}})); 
// app.use(session({secret:'secretKey'}));


// ================================================================
// setup routes
// ================================================================
routes(app);

// ================================================================
// start our server
// ================================================================
app.listen(port, function() {
    console.log('Server listening on port ' + port + '...');
});