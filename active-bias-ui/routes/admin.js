var crypto = require('crypto');
var adminDAO = require("../dao/AdminDAO");
var bodyParser = require('body-parser');
var session = require('express-session');
var util = require("../libraries/Utility")
var adminModel = require("../models/adminModel")
var unirest = require("unirest")


var urlencodedParser = bodyParser.urlencoded({ extended: false })



module.exports = function(app){
    

    app.get('/', function(req, res) {
        res.render('pages/index', { message: ''});
    });

    app.get('/home', function(req, res) {
        res.render('pages/homepage', { message: ''});
    });

    app.post('/home',urlencodedParser, function(req, res){
        var amount = req.body.amount;
        var month = req.body.month;
        var balance = req.body.balance;
        var day = req.body.day;
        var age = req.body.age;
        var spend_power = req.body.spend_power;
        var accountNumber = req.body.account_number
        var error = '';

        if (amount == '') error.push("Please enter amount")
        if (accountNumber == '') error.push("Please enter Account Number")
        if (balance == '') error.push("Please enter balance")
        if (day == '') error.push("Please enter day")
        if (age == '') error.push("Please enter age")
        if (spend_power == '') error.push("Please enter spend_power")

        if (error.length == 0){

            unirest.post('http://9c9c4196.ngrok.io/activeBiass/sendProfile')
            .headers({'Accept': 'application/json', 'Content-Type': 'application/json'})
            .send({ "accountNumber": accountNumber, "amount": amount, "balance": balance, "month": month,  "day": day, "age": age, "spend_power": spend_power })
            .end(function (response) {
                var response_data = response.body.response;
                return res.render('pages/dashboard.ejs', {message: {api_data: response_data, user_data: req.session.user_data }})
            });
        }else{
            return res.render('pages/dashboard.ejs', {message: {error_msg: error, user_data: req.session.user_data }})
        }


    } )


    app.get('/login', function(req, res) {
        res.render('pages/login', { message: ''});
    });


     app.post('/login', urlencodedParser, function(req, res){
        var error = [];
        var user_data = '';
        var usernameData = req.body.username,
            passwordData = req.body.password;
        if (usernameData=='') error.push('Please enter username');
        if (passwordData=='') error.push('Please enter password');
        if (error.length == 0){
            adminModel.findOne({"conditions":{"email": usernameData}}, function (response, err){
                if (!response){
                    return res.render('pages/login.ejs', {message: {error_msg:"User does not exist"}})
                }else{
                    if (response.password === util.md5(passwordData)){
                        req.session.user_data = response;
                    res.redirect('/home');
                }else{
                    return res.render('pages/login.ejs', {message: {error_msg:'Username or Password Incorrect'}})
                }
            }
        })
    }else{
         return res.render('pages/login.ejs', {message: {error_msg:error}})
        }
    });


    app.get('/register', function(req, res){
        res.render('pages/register.ejs', {message:{message: {error_msg:''}}});
    });


    app.post("/register", urlencodedParser, function (req, res) {
        adminDAO.admin_register(req.body,function(response){
        if (response.error){
            return res.render("pages/register.ejs", {message:{error_msg:response.message}})
        }else{
            return res.render("pages/login.ejs", {message:{success_msg:response.message}})

        }
    });
});

    app.get('/dashboard', function(req, res){
        console.log('req.session', req.session)
        res.render('pages/dashboard.ejs', {message: {user_data: req.session.user_data}});
    });

    app.get('/logout', function (req, res) {
        req.session.destroy();
        return res.render('pages/welcome.ejs', {message: {info_msg:'Thank you for your time'}});
    });

}