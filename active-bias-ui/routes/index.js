'use strict';

module.exports = function(app) {
    app.get('/', function(req, res) {
        res.render('pages/login');
    });

    app.get('/homess', function(req, res) {
        res.render('pages/home');
    });

    app.get('/loginss', function(req, res) {
        res.render('pages/login');
    });

    app.get('/aboutss', function(req, res) {
        res.render('pages/about');
    });
};