var _config = require('config.json')('./config/app.json');

var Connector = {

    _mongo: null,
    _rabbit: null,

    MongoDB: function(){
        if(Connector._mongo == null){
            var mongoose = require('mongoose');
            Connector._mongo = mongoose.connection;

            Connector._mongo.on('error',console.error);
            Connector._mongo.once('open', function(){

            });
            var url = 'mongodb://'+ _config.mongodb.username+':'+_config.mongodb.password+'@'+ _config.mongodb.host+':'+_config.mongodb.port+'/'+_config.mongodb.db;

            mongoose.connect(url);
        }

        return Connector._mongo;
    }
};

module.exports = Connector;