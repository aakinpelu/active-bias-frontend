var Connector = require('./Connector');
var _config = require('config.json')('./config/app.json');

var Promise = require("bluebird");
Promise.promisifyAll(require("mongoose"));

var Mongo = {

    _client: Connector.MongoDB(),

    _get: function(model,params,callback){
        var query = model.findOne(params.conditions);

        if(params.fields)
            query.select(params.fields);

        query.exec(function(err,model_data){
            if(err) return callback(Mongo.handleError(err));
            else
                return callback(model_data);
        });
    },

    _get_bulk: function(model,params,callback){

        if(!params.limit)
            params.limit = _config.query_limit;

        var query = model.find(params.conditions).limit(params.limit);

        if(params.sort)
            query.sort(params.sort);

        if(params.fields)
            query.select(params.fields);


        query.exec(function(err,model_data){
            if(err) return callback(Mongo.handleError(err));
            else
                return callback(model_data);
        });
    },


    _aggregriate: function(model,params,callback){
        var query = model.aggregate(params.conditions);

        query.exec(function(err,model_data) {
            if(err) return callback(Mongo.handleError(err));
            else
                return callback(model_data);
        });
    },

    _save: function(model,callback){
        model.save(function(err,data){
            if (err != null)
                return callback(Mongo.handleError(err));
            else
                return callback(data);
        });
    },

    // _delete: function(model, condition, data, callback){
    //     model.DeleteOne(condition, {$set:data}, {new:true}, function(err, resp){
    //         if (err)
    //             return callback(Mongo.handleError(err));
    //         else
    //             return callback(resp)
    //     });
    // },

  
    _update: function(model,condition,data,callback){
        model.findOneAndUpdate(condition,{$set:data},{new:true},function(err,resp){//{multi: true},
            if (err)
                return callback(Mongo.handleError(err));
            else
                return callback(resp);
        });
    },

      _delete: function(model,condition, callback){
        model.findOneAndRemove(condition, function(err,resp){
            if (err!=null)
                return callback(Mongo.handleError(err));
            else
                return callback(resp)
        })
    },


    _update_bulk: function(model,condition,data,callback){
        model.update(condition,{$set:data},function(err,resp){//{multi: true},
            if (err)
                return callback(Mongo.handleError(err));
            else
                return callback(resp);
        });
    },

    _search: function(model,condition,options,callback){
        model.search(condition,options,function(err,resp){
            if (err)
                return callback(Mongo.handleError(err));
            else
                return callback(resp.hits);
        });
    },

    close: function() {
        this._client.close();
    },

    handleError: function(report){
        return {"error":true,"message":report};
    }


};

module.exports = Mongo;