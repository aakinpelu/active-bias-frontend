var crypto = require('crypto');


var Utility = {

    title_case: function(text){
        var change_case = require('change-case');
        var lower = change_case.lowerCase(text);
        return change_case.upperCaseFirst(lower);
    },

    msisdn_sanitizer: function(msisdn,plus){
        if(msisdn) {
            msisdn = msisdn.replace(/\s+/g, '');
            msisdn = msisdn.replace('+', '');

            if (!isNaN(msisdn)) {
                if (msisdn.match(/^234/i)) msisdn = '0' + msisdn.substr(3);

                if (msisdn.length == 11) {
                    msisdn = '+234' + msisdn.substr(1);

                    if (!plus)  msisdn = msisdn.replace('+', '');

                    return msisdn;
                } else {
                    return "";
                }
            } else
                return "";
        }else
            return "";

    },

    

    random_int: function (min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    },

       
    md5:    function (str) {
	return crypto.createHash('md5').update(str).digest('hex');
    },

    auth: function (req, res, next) {
        if (req.session && req.session.school_data){
            modelname.findOne({"conditions":{"email": req.session.school_data.email}}, function (response, err){
               if (!response){
                   req.session.reset();
                   res.redirect('/school-login');
               } else{
                   next();
               }
            });

            }else{
                res.redirect('/school-login');
            }
    }


};
module.exports = Utility;