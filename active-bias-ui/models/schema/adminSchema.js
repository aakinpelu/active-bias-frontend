var mongoose = require('mongoose');
var mongoosastic = require('mongoosastic');
var _config = require('config.json')('./config/app.json')
var _collection = _config.mongodb.collections;


var Schema = mongoose.Schema;

var adminSchema = new Schema({
    user_id: {type: Number},
    email: {type: String, unique: true},
    fullname: {type: String},
    username: {type: String},
    password: {type: String},
    phone: {type: Number},
    profile_pics: {type: String, default:'default.png'},
    address: {type: String},
    email_confirm_status: {type: Number, default:0},
    phone_confirm_status: {type: Number, default: 0},
    facebook_id: {type: String, default:0},
    twitter_id: {type: String, default:0},
    google_id: {type: String, default: 0},
    email_sent: {type: String, default:0},
    phone_sent: {type: Number, default:0},
    date_added: {type: Date, default: Date.now},
});

var AdminModel = mongoose.model(_collection.admin, adminSchema);

module.exports = AdminModel;