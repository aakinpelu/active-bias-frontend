var _config = require('config.json')('./config/app.json');
var adminSchema = require('./schema/adminSchema');
var Mongo = require('../libraries/Mongo');


var AdminModel = {
    save: function (data, callback) {
        var user = adminSchema(data);
        Mongo._save(user, function (state) {

            if (state.error){
                return callback(state);
            }else {
                return callback(state);
            }
        });

    },

    update: function (data,condition,callback){

        Mongo._update(adminSchema,condition,data,function(state){
            return callback(state);
        });
    },

    findAggregate: function(param,callback){
        Mongo._aggregriate(adminSchema, param, function (data) {
            return callback(data);
        });
    },

    findAll: function (param, callback) {
        Mongo._get_bulk(adminSchema, param, function (data) {
            return callback(data);
        });
    },
    delete: function (data,condition,callback){
        Mongo._delete(schoolSchema, condition, data, function(state){
            return callback(state);
        });
    },

    findOne: function (param, callback) {
        Mongo._get(adminSchema, param, function (data) {
            return callback(data);
        });
    },

    search: function (param,callback){
        var options = {}
        if(param.from) options.from = param.from
        if(param.size) options.size = param.size
        if(param.sort) options.sort = param.sort
        Mongo._search(adminSchema,param.query,options,function(data){
            return callback(data);
        })
    }
};

module.exports = AdminModel;