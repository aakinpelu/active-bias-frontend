var crypto = require('crypto');
var adminModel = require('../models/adminModel');
var bodyParser = require('body-parser');
var session = require('express-session');
var urlencodedParser = bodyParser.urlencoded({ extended: false });
var util = require("../libraries/Utility");
var Resp = require('../dao/Response');
var _config = require('config.json')('../../config/app');




var AdminDAO = {
    admin_register: function(param, callback){
        var error = [];
        var data = {};
        console.log('param', param)
        
        if (param.fullname=='') error.push('Fullname missing');
        if (param.email=='') error.push('Email Missing');
        if (param.username =='' || param.username.length < 6) error.push('Username is too short');
        if (param.password  =='') error.push('Password Missing');
        if (param.cpass =='') error.push('Please confirm password');
        if (param.password  != param.cpass) error.push('Passwords do not match');
        
        if (error.length == 0){
            var data = {'fullname': param.fullname,'email': param.email,'username': param.username,'password': util.md5(param.password)}
            adminModel.findOne({"conditions":{"email": param.email}}, function (response){
                if (response){
                    return callback(Resp.error({msg:"User already exists!!!"}));
                }
            adminModel.save(data, function(in_resp){
                if (in_resp != null && !in_resp.error){
                     return callback(Resp.success({msg:"Your account has been successfully created!",resp:in_resp}));

                }else{
                    return callback(Resp.error({msg:"Error creating user!"}));
                }

            })

            })

        }else{
            return callback(Resp.error({msg:error}));

        }
    },

    admin_login: function(param, callback){
        var error = [];
        if (param.username=='') error.push('Please enter username');
        if (param.password=='') error.push('Please enter password');

        if (error.length == 0){
            adminModel.findOne({"conditions":{"email": param.username}}, function (response){
                if (!response){
                    return callback(Resp.error({msg:'User does no exist'}));
                }else{
                    if (response.password === util.md5(param.password)){
                        return callback(Resp.success({msg:'Logged In, Session started'}))
                    }else{
                        return callback(Resp.error({msg:'Username or password incorrect'}))
                    }
                }
            })
        }else{
            return callback(Resp.error({msg:error}));
        }

    },

    admin_logout: function(callback){
        session.destroy();
        return callback(Resp.success({msg:''}))
    },


    auth: function(callback){
        if (session && session.user_data){
            adminModel.findOne({"conditions":{"email": session.user_data.email}}, function (response, err){
                if (!response){
                    session.reset();
                    return callback(Resp.error({msg:''}))
                }else{
                    return callback(Resp.success({msg:''}))
                }
            })
        }else{
            return callback(Resp.error({msg:''}))
        }
    },

    
};

module.exports = AdminDAO;



